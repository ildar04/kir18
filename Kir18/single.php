  <?php 
/* Загрузка страницы о ученике Школы Теле Мастерства*/
?>   

<?php 
get_header();
?>   

<?php 
    // Получаем пост для использования его функций (например, the_content())
    the_post();
    
    $categories = get_the_category($post->ID);
    $catName = $categories[0]->name;
    $parentCatName = get_cat_name($categories[0]->parent);

    if($parentCatName == "Наши ученики"){

?>
<section class="user section-block">
    <div class="section-title">
        <h2>
            <?php the_title(); ?>
        </h2>
    </div>
    <article class="user__block clearfix">
        <figure class="user__pic">
            <img <?php first_image_from_post(get_the_content()) ?> alt="<?php the_title(); ?>">
        </figure>
        <ul class="user__list-info">
            <li><b>Дата рождения: <?php echo (get_post_meta($post->ID, 'birth_date', true));?></b></li>
        </ul>
        <div class="user__description">
            <p>
                <?php 
                $clean_content = strip_tags(get_the_content());
                echo $clean_content; ?>
            </p>
        </div>
    </article>
    <br>    <br>

</section>

<?php
    }
    else if ($catName == "Газета"){
?>
<section class="user section-block">
    <div class="section-title">
        <h2>
            <?php the_title(); ?>
        </h2>
    </div>
    <article class="user__block clearfix">
        <figure class="user__pic">
            <img src="<?php echo get_image_to_post(get_the_ID(), 'large'); ?>" alt="Изображение">
        </figure>
        <ul class="user__list-info">
            <li><b><?php the_excerpt(); ?></b></li>
        </ul>
        <div class="user__description">
            <p>
                <?php 
                $clean_content = strip_tags(get_the_content());
                echo $clean_content; ?>
            </p>
        </div>
    </article>
    <?php
        $video_name = get_post_meta($post->ID, 'video', true);
        if ($video_name != '')
        {
    ?>
    <script>
        $(function () {
            $('.video').video();
        });
    </script>
    <div class="section-title">
        <h3>
            Для просмотра видео нажмите на изображение:
        </h3>
    </div>
    <article class="video" data-video-id="<?php echo $video_name; ?>" style="margin: auto; display: block;">
        <figure class="video__preview" style="margin: 0 auto;">
            <img src="http://img.youtube.com/vi/<?php echo $video_name; ?>/mqdefault.jpg" alt="">
        </figure>
        <div class="video__modal">
            <div class="video__player"></div>
        </div>
    </article>
    <?php 
        }
    ?>
    <?php 
        $images = get_all_image_URLs_from_content(get_the_content());
        for($i = 1; $i < count($images); $i++ ){
    ?>
        <figure class="">
                <img src="<?php echo $images[$i]; ?>" alt="Изображение" style="margin: auto; display: block;">
        </figure>
    <?php
        }
    ?>
    <br>
    <div class="prev-next-links">
        <?php 
            if( get_adjacent_post(true, '', true) ) { 
                previous_post_link('%link', '← Предыдущая новость', true);
            }
            echo "  ";
            if( get_adjacent_post(true, '', false) ) { 
                next_post_link('%link', 'Следующая новость →');
            }                             
        ?>
    </div>
    <br>
    <br>

</section>
<?php
    }
?>

<?php
get_footer(); 
?>