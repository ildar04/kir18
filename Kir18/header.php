<?php 
function baseDirectory() {
echo bloginfo('template_url');
}
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="yandex-verification" content="9cb9d42396871f96" />
    <?php
    wp_site_icon();
    ?>
    <title><?php echo wp_get_document_title(); ?></title>
    <!-- bower:css -->
    <link rel="stylesheet" href="<?php baseDirectory(); ?>/css/normalize.css" />
    <link rel="stylesheet" href="<?php baseDirectory(); ?>/css/owl.carousel.min.css" />
    <link rel="stylesheet" href="<?php baseDirectory(); ?>/css/notosans-fontface.min.css" />
    <link rel="stylesheet" href="<?php baseDirectory(); ?>/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?php baseDirectory(); ?>/css/owl.theme.default.min.css" />
    <link rel="stylesheet" href="<?php baseDirectory(); ?>/css/styles.css" />
    <link rel="stylesheet" href="<?php baseDirectory(); ?>/css/i-css.css" />
    <link rel="stylesheet" href="<?php baseDirectory(); ?>/css/easydropdown.css">
    
    <!-- bower:js -->
    <script src="<?php baseDirectory(); ?>/js/jquery.min.js"></script>
    <script src="<?php baseDirectory(); ?>/js/owl.carousel.min.js"></script>
    <!-- endbower -->
    <script src="<?php baseDirectory(); ?>/js/lib.js"></script>
    <script src="<?php baseDirectory(); ?>/js/ajax.js"></script>
    
</head>
<body>
    <?php $all_options = get_option('true_options'); // это массив Параметров в Настройках сайта ?>
    <header class="header">        
        <div class="header__background" style="background-image: url(<?php baseDirectory(); //header_image(); ?>/img/main-bg.jpg)">
            <div class="curtain">
                <a href="#" class="header__mobile-btn"><i class="fa fa-bars" aria-hidden="true"></i></a>
                <nav class="header__main-nav">
                    <a href="#" class="header__mobile-btn"><i class="fa fa-close" aria-hidden="true"></i></a>
                <?php
			    if ( function_exists( 'wp_nav_menu' ) )
			        wp_nav_menu( 
				        array( 
				        'theme_location' => 'custom-menu',
				        'fallback_cb'=> 'custom_menu',
				        'container' => 'ul',
				        'menu_id' => 'header__menu',
				        'menu_class' => 'header__menu') 
					);
			    else custom_menu();
				?>
                </nav>
                <a href="/" class="header__logo">
                    <img src="<?php baseDirectory(); ?>/img/logo.png" alt="{logo}">
                </a>
                <div class="header__title-logo">
                    <h1>
                        <?php echo $all_options['header__title-logo']; ?>
                    </h1>
                </div>
                <div class="header__contact-info">
                    <span><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;
                        <?php echo $all_options['phone_number']; ?></span><br>
                    <span><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;<?php echo $all_options['address']; ?></span>
                </div>
            </div>
        </div>
    </header>