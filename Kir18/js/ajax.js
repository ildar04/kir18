jQuery(document).ready(function ($) {
    $('#my-form').submit(function () {
        
        document.querySelector('input[type="submit"]').disabled = true;

        var nameVal = $('input[name="name"]').val();
        var emailVal = $('input[name="email"]').val();
        var messageVal = $('textarea[name="message"]').val();
        data = {
            action: 'myaction',
            link: $('#my-form').serialize(),
            name: nameVal,
            email: emailVal,
            message: messageVal,
        };
        var ajaxurl = "/wp-admin/admin-ajax.php";
        var ajaxurl = "/wp-content/themes/Kir18/sendMail.php";

        var request = $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: data
        });
        request.always(function (response) {
            alert(response);
            document.querySelector('input[type="submit"]').disabled = false;
        });
        return false;
    });
});