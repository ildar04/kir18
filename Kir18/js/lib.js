$(function () {
    $('.header__mobile-btn').on('click', function () {
        $('body').toggleClass('open-menu');
    });

    $('.features').addClass('features_active');

    /*$(document).on('scroll', function () {
        var scroll = $(document).scrollTop();
        if(scroll >= 450) {
            $('.examples').addClass('examples_active');
        }

        if(scroll >= 650) {
            $('.reviews').addClass('reviews_active');
        }
    });*/

    $('#features, #reviews').owlCarousel({
        autoHeight: true,
        loop: false,
        nav: false,
        dots: true,
        responsive: {
            0: {
                items:1
            },
            600: {
                items:2
            },
            960: {
                dots: false,
                items: 3,
                touchDrag: false
            }
        }
    });
});

(function ($) {
    $.fn.video = function (options) {
        options = $.extend({}, options);
        options.padding = options.padding || 80;
        options.maxWidth = options.maxWidth || 960;
        options.ratio = options.ratio || [16,9];

        var video = function () {

            $(this).children('.video__preview').find('img').on('click', function () {
                var width = $(document).width() - options.padding;
                width = width < options.maxWidth ? width : options.maxWidth;
                var parent = $(this).parent().parent(),
                    player = parent.children('.video__modal').children('.video__player'),
                    iframe = $('<iframe/>', {
                        frameborder: 0,
                        id: $(this).data('video-id'),
                        src: 'https://www.youtube.com/embed/' + parent.data('video-id'),
                        allowfullscreen: 'allowfullscreen',
                        width: width,
                        height: width/options.ratio[0] * options.ratio[1]
                    });

                player.append(iframe);
                player.css({
                    'margin-left': -(width/2) + "px",
                    'margin-top': -((width/options.ratio[0] * options.ratio[1]) / 2) + "px"
                });
                parent.children('.video__modal').toggleClass('video__modal_active');
            });

            $(this).children('.video__modal').on('click', function () {
                $(this).toggleClass('video__modal_active').children('.video__player').text('');
            });
        };
        return this.each(video);
    };
})(jQuery);
(function ($) {
        $.fn.videoIndex = function (options) {
        options = $.extend({}, options);
        options.padding = options.padding || 80;
        options.maxWidth = options.maxWidth || 960;
        options.ratio = options.ratio || [16,9];

        var video = function () {

            $(this).children('.video__preview').find('img').on('click', function () {
                var width = $(document).width() - options.padding;
                width = width < options.maxWidth ? width : options.maxWidth;
                var parent = $(this).parent().parent(),
                    player = $('#video__modal').children('.video__player'),
                    iframe = $('<iframe/>', {
                        frameborder: 0,
                        id: $(this).data('video-id'),
                        src: 'https://www.youtube.com/embed/' + parent.data('video-id'),
                        allowfullscreen: 'allowfullscreen',
                        width: width,
                        height: width/options.ratio[0] * options.ratio[1]
                    });

                player.append(iframe);
                player.css({
                    'margin-left': -(width/2) + "px",
                    'margin-top': -((width/options.ratio[0] * options.ratio[1]) / 2) + "px"
                });
                $('#video__modal').toggleClass('video__modal_active');
            });

            $('#video__modal').on('click', function () {
                $(this).toggleClass('video__modal_active').children('.video__player').text('');
            });
        };
        return this.each(video);
    };
})(jQuery);
