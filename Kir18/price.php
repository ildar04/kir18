<?php
/*
Template Name: Прайслист
*/
?>

<?php 
get_header();
?>   

<section class="section-block">
    <div class="section-title">
        <h2>
            Цены
        </h2>
    </div>
    <article class="price price__dark">
        <figure class="price__image">
            <img src="<?php baseDirectory(); ?>/img/notepad.png" alt="{name}">
            <figcaption class="price__caption">
                <h2>Тележурналистика</h2>
            </figcaption>
        </figure>
        <div class="price__description">
            <ul class="price__desc-list">
                <li>
                    <i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;МЕСЯЦ ЗАНЯТИЙ: 4 ЗАНЯТИЯ по 2 ак/час =
                    8 ак/час + репетиции и съемки в Телекомпании + монтажи
                </li>
                <li>
                    <i class="fa fa-money" aria-hidden="true"></i>&nbsp;3500 руб/мес
                </li>
                <li>
                    <i class="fa fa-users" aria-hidden="true"></i>&nbsp;Разбивка групп по возрасту
                </li>
                <li>
                    <i class="fa fa-free-code-camp" aria-hidden="true"></i>&nbsp;Каникулы - август
                </li>
            </ul>
            <p>
                Занимаемся круглогодично. в июне сдаем тесты, получаем свидетельства и сертификаты. Полный цикл работы
                телекомпании + обязательное участие в программе Свободный Формат.
            </p>
        </div>
    </article>
    <article class="price">
        <figure class="price__image">
            <img src="<?php baseDirectory(); ?>/img/microphone.png" alt="{name}">
            <figcaption class="price__caption">
                <h2>Речь</h2>
            </figcaption>
        </figure>
        <div class="price__description">
            <ul class="price__desc-list">
                <li>
                    <i class="fa fa-money" aria-hidden="true"></i>&nbsp;12000 руб/10 полных часов БАЗОВЫЙ КУРС РЕЧЕВЫХ
                    ТЕХНИК (индивидуально)
                </li>
            </ul>
            <p>
                Можно заниматься и в группе (до 5ти человек - ТОГДА СУММА РАЗБИВАЕТСЯ НА ВСЕХ)
            </p>
        </div>
    </article>
    <article class="price price__dark">
        <figure class="price__image">
            <img src="<?php baseDirectory(); ?>/img/people.png" alt="{name}">
            <figcaption class="price__caption">
                <h2>Счастливые люди</h2>
            </figcaption>
        </figure>
        <div class="price__description">
            <ul class="price__desc-list">
                <li>
                    <i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;краткосрочно/круглогодично/по субботам с 09.00
                </li>
                <li>
                    <i class="fa fa-money" aria-hidden="true"></i>&nbsp;5000 руб/мес
                </li>
            </ul>
            <p>
                Участие в программе «Свободный Формат» по мере готовности и желанию /съемки +репетиции/без монтажа
            </p>
        </div>
    </article>
</section>

<?php
get_footer(); 
?>