<?php
/*
Template Name: Блоги
*/
?>
  <?php 
/* Загрузка страницы Свободный формат*/
?>   

<?php 
get_header();
?>   

<script>
    $(function () {
        $('.video').video();
    });
</script>

<section class="gallery">
    <div class="section-title">
        <h2>
            Блоги
        </h2>
    </div>
    <?php
    $Path=$_SERVER['REQUEST_URI'];
$URI=get_permalink();
    ?>
    <form align="center" width="100" action="<?php echo $URI; ?>" id="video-form" method="GET" >
        <select name="yearCr" id="yearCr" class="dropdown">
            <option value="">Выберите год</option>
            <?php
            $yearPrint = 2017;
            while ($yearPrint <= date('Y')){
                echo '<option value="'.$yearPrint.'">'.$yearPrint.'</option>';
                $yearPrint++;
            } 
            ?>
        </select>     
        <select  name="month" id="month" class="dropdown">
            <option value="">Выберите месяц</option>
            <option value="1">Январь</option>
            <option value="2">Февраль</option>
            <option value="3">Март</option>
            <option value="4">Апрель</option>
            <option value="5">Май</option>
            <option value="6">Июнь</option>
            <option value="7">Июль</option>
            <option value="8">Август</option>
            <option value="9">Сентябрь</option>
            <option value="10">Октябрь</option>
            <option value="11">Ноябрь</option>
            <option value="12">Декабрь</option>
        </select>
            <input type="submit" class="i-btn" value="Найти">
    </form>
    <script>
    function $_GET(key) {
        var s = window.location.search;
        s = s.match(new RegExp(key + '=([^&=]+)'));
        return s ? s[1] : false;
    }
    var yearr = $_GET('yearCr');
    if (yearr){
        $("select#yearCr").val(yearr);
    }
    var month = $_GET('month');
    if (month){
        $("select#month").val(month);
    }
    </script>
<?php 
$year = $_GET['yearCr'];
//echo '<br>---------------'.$year.'---------------';
$month = $_GET['month'];
$args = 'tag=Блог'.'&showposts=6'.'&paged='.$paged;
if (isset($year)) { 
    $args .= '&year='.$year; 
}
if (isset($month)) { 
    $args .= '&monthnum='.$month; 
}
$wp_query = new WP_Query( $args );

while ( $wp_query->have_posts() ) { $wp_query->the_post();
?>
    <article class="video" data-video-id="<?php the_content();?>">
        <figure class="video__preview">
            <img src="http://img.youtube.com/vi/<?php the_content();?>/mqdefault.jpg" alt="<?php the_title();?>">
        </figure>
        <div class="video__modal">
            <div class="video__player"></div>
        </div>
        <div width="50"><?php the_title();?></div>
    </article>
    <?php
}
if (!$wp_query->have_posts() ){
    echo '<h1 align="center">Нет видеозаписей</h1>';
}
?>
<?php
    wp_ildar_pagination();
?>
</section>

<?php
get_footer(); 
?>