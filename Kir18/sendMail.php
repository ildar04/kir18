<?php
// проверка на спам - просто прерываем выполнение кода, при желании можно и сообщение спамерам вывести
//if( isset( $_POST['name'] ) || isset( $_POST['message'] ) )
//	exit;
 
// подключаем WP
require_once '../../../wp-load.php';
$response = "Заполните все поля!";
$headers;
// следующий шаг - проверка на обязательные поля, у нас это емайл, имя и сообщение
if( isset( $_POST['name'] ) && isset( $_POST['email'] ) && isset( $_POST['message'] ) ) {
 
	$headers = array(
		"Content-type: text/html; charset=utf-8",
		"From: " . $_POST['name'] . " <" . trim($_POST['email']) . ">",
        "Reply-To: " . trim($_POST['email'])
	);
    
    $admin_email = get_option('admin_email');//'ildar04v@gmail.com'
	if( wp_mail( $admin_email, 'Сообщение с сайта от '.$_POST['email'], wpautop( $_POST['message'] ), $headers ) ) {
        $response = "Сообщение отправлено успешно!";
	}
    else {
        $response = "Произошла ошибка. Пожалуйста, свяжитесь другими способами.";
    }
 
}
 echo $response;
exit;
?>