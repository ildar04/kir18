
<?php
/*
Template Name: Телегазета список
*/
?>

<?php 
get_header();
?>   

 <section class="users section-block">
    <div class="section-title">
        <h2>
            Телегазета
        </h2>
    </div>   
<?php 
$cat_ID = get_cat_ID('Газета');
$args = 'cat='.$cat_ID.'&showposts=6&paged='.$paged;
$wp_query = new WP_Query( $args );
while ( $wp_query->have_posts() ) { $wp_query->the_post();
?>
<section class="user section-block">
    <div class="section-title">
        <a href="<?php the_permalink(); ?>">
            <h2>
                <?php the_title(); ?>
            </h2>
        </a>
    </div>
    <article class="user__block clearfix">
        <a href="<?php the_permalink(); ?>">
            <figure class="user__pic">
                <img src="<?php echo get_image_to_post(get_the_ID(), 'medium'); ?>" alt="Изображение">
            </figure>
        </a>
        <ul class="user__list-info">
            <a href="<?php the_permalink(); ?>">
                <li><b><?php the_excerpt(); ?></b></li>
            </a>
        </ul>
    </article>
    <br>    <br>

</section>
     <?php
                                  }
?>
</section>
<?php
    wp_ildar_pagination();
?>

<?php
get_footer(); 
?>