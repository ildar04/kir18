  <?php 
/* Загрузка страницы о ученике Школы Теле Мастерства*/
?>   

<?php 
get_header();
?>   

<?php 
    // Получаем пост для использования его функций (например, the_content())
    the_post();
    
    $categories = get_the_category($post->ID);
    $catName = $categories[0]->name;
    $parentCatName = get_cat_name($categories[0]->parent);

    if($parentCatName == "Наши ученики"){

?>
<section class="user section-block">
    <div class="section-title">
        <h2>
            <?php the_title(); ?>
        </h2>
    </div>
    <article class="user__block clearfix">
        <figure class="user__pic">
            <img <?php first_image_from_post(get_the_content()) ?> alt="<?php the_title(); ?>">
        </figure>
        <ul class="user__list-info">
            <li><b>Дата рождения: <?php echo (get_post_meta($post->ID, 'birth_date', true));?></b></li>
        </ul>
        <div class="user__description">
            <p>
                <?php 
                $clean_content = strip_tags(get_the_content());
                echo $clean_content; ?>
            </p>
        </div>
    </article>
<!-- Убрать "Работы"

    <script>
        $(function () {
            $('.video').video();
        });
    </script>
    <div class="section-title">
        <h2>
           Работы
        </h2>
    </div>

<?php 

$cat = $categories[0]->term_id;
$args = 'tag=СФ&cat='.$cat.'&showposts=6'.'&paged='.$paged;
$wp_query = new WP_Query( $args );
while ( $wp_query->have_posts() ) 
    { $wp_query->the_post();
        $posttags = get_the_tags(); 
?>
    
    <article class="video" data-video-id="<?php the_content();?>">
        <figure class="video__preview">
            <img src="http://img.youtube.com/vi/<?php the_content();?>/mqdefault.jpg" alt="<?php the_title();?>">
        </figure>
        <div class="video__modal">
            <div class="video__player"></div>
        </div>
    </article>
    <?php   }   ?>
    
<?php
    wp_ildar_pagination();
?>
-->
    <br>    <br>

</section>

<?php
    }
    else if ($catName == "Газета"){
?>
        <section class="user section-block">
    <div class="section-title">
        <h2>
            <?php the_title(); ?>
        </h2>
    </div>
    <article class="user__block clearfix">
        <figure class="user__pic">
            <img <?php first_image_from_post(get_the_content()) ?> alt="<?php the_title(); ?>">
        </figure>
        <ul class="user__list-info">
            <li><b>Отрывок: <?php echo (get_post_meta($post->ID, 'birth_date', true));?></b></li>
        </ul>
        <div class="user__description">
            <p>
                <?php 
                $clean_content = strip_tags(get_the_content());
                echo $clean_content; ?>
            </p>
        </div>
    </article>
    <br>    <br>

</section>
<?php
    }
?>

<?php
get_footer(); 
?>