<?php 
//$all_options = get_option('true_options'); 
// это массив Параметров в Настройках сайта ?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter43861979 = new Ya.Metrika({
                    id:43861979,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/43861979" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<style>
.alert-news
{
	background: rgba(39,39,39,.5);
	color: #fff;
	cursor: pointer;
	left: 0;
	padding: 5px 10px;
	position: fixed;
	top: 50%;
}

.md-overlay
{
	background: rgba(39,39,39,.5);
	display: none;
	height: 100%;
	left: 0;
	position: fixed;
	top: 0;
	width: 100%;
}

.md-modal
{
	background: rgb(39, 39, 39);
    	color: #fff;
	display: none;
	left: 50%;
	margin-left: -250px;
	margin-top: -250px;
	min-width: 500px;
	position: fixed;
	top: 50%;
	z-index: 999;
}
.md-content
{
	padding: 15px;
}
.md-content button 
{
	background: rgba(255,255,255,0.5);
    	border: none;
    	text-transform: uppercase;
    	padding: 5px;
	color: #fff;
}
.md-active
{
	display: block;
}
</style>
<script>
$(function(){
$('.md-trigger, .md-close').on('click', function() {
	$('#' + $(this).data('modal')).toggleClass('md-active');
	$('.md-overlay').toggleClass('md-active');
});
})
function resetYouTube() {
	var adv = $('#adv');
	adv.attr('src', adv.attr('src'));
}
</script>
<div class="alert-news md-trigger" data-modal="modal-1">
	<p>Запись на новый сезон</p>
</div>
<div class="alert-news md-trigger" style="margin-top: 70px;" data-modal="modal-2">
	<p><img src="http://img.youtube.com/vi/fYjlE4UDU4o/mqdefault.jpg" alt="Реклама" width="160"></p>
</div>
<div class="md-modal md-effect-1" id="modal-1">
	<div class="md-content">
		<h3>Запись на новый сезон</h3>
		<div>
			<p>+7 (9128) 565-089</p>
			<p>В студии телевезионного мастерства открыта запись на новый сезон</p>
			<p>Направления</p>
			<ul>
				<li>Телеведущие и телерепортеры</li>
				<li>Телевезионная режиссура</li>
				<li>Спортивный репортаж</li>
			</ul>
			<p>Все слушатели студии принимают участие в программе «СВОБОДНЫЙ ФОРМАТ»!</p>
			<button class="md-close" data-modal="modal-1">Закрыть</button>
		</div>
	</div>
</div>
<div class="md-modal md-effect-1" id="modal-2">
	<div class="md-content">
		<div>
			<iframe width="560" height="315" src="https://www.youtube.com/embed/fYjlE4UDU4o" frameborder="0" allowfullscreen id="adv"></iframe>
		</div>
		<button class="md-close" data-modal="modal-2" onclick="resetYouTube()">Закрыть</button>
	</div>
</div>
<div class="md-overlay"></div>
</body>
</html>