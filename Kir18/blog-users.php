
<?php
/*
Template Name: Наши ученики
*/
?>

<?php 
get_header();
?>   

 <section class="users section-block">
    <div class="section-title">
        <h2>
            Наши ученики
        </h2>
    </div>   
<?php 
$parent_cat_ID = get_cat_ID('Наши ученики');
$categories;
$childs = get_categories('child_of='.$parent_cat_ID);
     foreach ($childs as $child):
         if($parent_cat_ID == $child->category_parent) :
            $categories .=  $child->cat_ID.',';
         
         else:
            $categories .= '-' . $child->cat_ID . ',';
         endif;
     endforeach;
     
// присваиваем переменной строку без последней запятой
$categories = substr($categories, 0, -1);
     
$args = 'cat='.$categories.'&showposts=6&paged='.$paged;
$wp_query = new WP_Query( $args );
while ( $wp_query->have_posts() ) { $wp_query->the_post();
?>
        <article class="users__block">
        <a href="<?php the_permalink(); ?>">
            <figure class="users__pic">
                    <img <?php first_image_from_post(get_the_content()); ?> alt="{user}">
                <figcaption class="users__name">
                        <h3><?php the_title();?></h3>
                </figcaption>
            </figure>
        </a>
    </article>
    <?php
}
?>
<?php
    wp_ildar_pagination();
?>
</section>

<?php
get_footer(); 
?>