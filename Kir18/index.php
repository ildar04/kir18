  <?php 
get_header();
?>   
<script>
    $(function () {
        $('.video').videoIndex();
    });
</script>
<div class="video__modal" id="video__modal" style="z-index: 10;">
    <div class="video__player" id="video__player"></div>
</div>
<section class="news features">
    <div class="section-title">
        <h2>
            Телегазета
        </h2>
    </div>
    
    <div class="owl-carousel owl-theme" id="features">
    <?php 
        $cat_ID = get_cat_ID('Колонка редактора');
        $args = 'cat='.$cat_ID.'&showposts=5&paged='.$paged;
        $wp_query = new WP_Query( $args );
        if ( $wp_query->have_posts() ) { $wp_query->the_post();
        ?>
        <div class="item">
            <article class="features__column">
                <header class="features__title">
                    <a href="<?php echo 'колонка-редактора/'; ?>">
                        <h3 class="features__border features__border_blue">Колонка редактора</h3>
                    </a>
                </header>
                <figure class="features__image">
                    <div class="features__icon">
                        <article class="video" data-video-id="<?php the_content();?>">
                            <figure class="video__preview">
                                <img src="http://img.youtube.com/vi/<?php the_content();?>/mqdefault.jpg" alt="<?php the_title();?>">
                            </figure>
                            <div class="video__modal">
                                <div class="video__player"></div>
                            </div>
                        </article>
                    </div>
                </figure>
                <div class="features__description">
                    Нажмите на изображение для просмотра видео
                </div>
            </article>
        </div>
    <?php 
                                      }
    ?>
        
        <?php 
        $cat_ID = get_cat_ID('Газета');
        $args = 'cat='.$cat_ID.'&showposts=5&paged='.$paged;
        $wp_query = new WP_Query( $args );
        while ( $wp_query->have_posts() ) { $wp_query->the_post();
        ?>
        <div class="item">
            <article class="features__column">
                <header class="features__title">
                    <a href="<?php the_permalink(); ?>">
                        <h3 class="features__border features__border_blue"><?php the_title(); ?></h3>
                    </a>
                </header>
                <figure class="features__image">
                    <div class="features__icon fds">
                        <a href="<?php the_permalink(); ?>">
                            <div style="background: url(<?php echo get_image_to_post(get_the_ID()); ?>);background-size: cover;" class="news-image"></div>
                        </a>
                    </div>
                </figure>
                <div class="features__description">
                    <a href="<?php the_permalink(); ?>">
                        <?php the_excerpt(); ?>
                    </a>
                </div>
            </article>
        </div>
        <?php 
                                          }
        ?>
    
    </div>
</section>

<section class="examples clearfix examples_active">
    <div class="section-title">
        <h2>
            Особенности
        </h2>
    </div>
    <?php
    $imageLinks = get_images_to_index();
    foreach($imageLinks as $imgLink){
    ?>
    <article class="examples__block">
        <figure class="examples__image" style="background-image: url(<?php echo $imgLink; ?>)"></figure>
    </article>
<?php } ?>
</section>
    <section class="reviews reviews_active">
        <div class="section-title section-title_white">
            <h2>
                Отзывы
            </h2>
        </div>
        <div class="owl-carousel owl-theme" id="reviews">
            <?php
            $comments = get_comments_custom();
            foreach($comments as $comment){
            ?>
            <div class="item">
                <article class="reviews__block">
                    <figure class="reviews__avatar">
                        <img <?php echo $comment->imgLink; ?> alt="{Reviewer}">
                        <figcaption class="reviews__name">
                            <h3><?php echo $comment->name; ?></h3>
                            <span class="reviews__job">
                                <?php echo $comment->title; ?>
                            </span>
                        </figcaption>
                    </figure>
                    <p class="reviews__description">
                        <?php echo $comment->content; ?>
                    </p>
                </article>
            </div>
            <?php } ?>
        </div>
    </section>  
<section class="contacts">
        <?php $all_options = get_option('true_options'); 
// это массив Параметров в Настройках сайта ?>
    <div class="section-title">
        <h2>
            Связаться с нами
        </h2>
        </div>
        <div class="contacts__block-form">
            <div class="contacts__info">
                <span><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;<?php echo $all_options['phone_number']; ?></span><br>
                <span><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;<?php echo $all_options['email']; ?></span><br>
                <span><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;<?php echo $all_options['address']; ?></span><br>
            </div>
            <div class="contacts__division">
                &mdash;ИЛИ&mdash;
            </div>
            <form action="" id="my-form" method="POST" >
                <div class="form-group">
                    <input type="text" name="name" class="input" placeholder="Ваше имя">
                </div>
                <div class="form-group">
                    <input type="email" name="email" class="input" placeholder="Ваш email">
                </div>
                <div class="form-group">
                    <textarea name="message" class="input" id="message" cols="30" rows="20" placeholder="Ваше сообщение"></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn_full-width" value="Отправить">
                </div>
            </form>
        </div>
        <div class="contacts__map">
            <script type="text/javascript" charset="utf-8" async
                    src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=v5m9sdYk_H19hqmPBYcLZZ_a5qnkweXk&amp;width=100%25&amp;height=370&amp;lang=ru_RU&amp;sourceType=constructor&amp;scroll=true">

            </script>
        </div>
    </section>
<?php
get_footer(); 
?>