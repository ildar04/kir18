<?php
//Произвольное меню
if ( function_exists( 'register_nav_menus' ) )
{
	register_nav_menus(
		array(
			'custom-menu'=>__('Custom menu'),
		)
	);
}

function custom_menu(){
	echo '<ul>';
	wp_list_pages('title_li=&');
	echo '</ul>';
}

add_theme_support( 'custom-header' );

include('contacts-settings.php');
      
/*
// функция регистирации сайтбары
function true_register_wp_sidebars() {
 
	// В боковой колонке - первый сайдбар 
	register_sidebar(
		array(
			'id' => 'youtube_sidebar', // уникальный id
			'name' => 'Сайдбар Ютуба', // название сайдбара
			'description' => 'Перетащите сюда виджеты, чтобы добавить их в сайдбар.', // описание
			'before_widget' => '<div id="%1$s" class="side widget %2$s">', // по умолчанию виджеты выводятся <li>-списком
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-title">', // по умолчанию заголовки виджетов в <h2>
			'after_title' => '</h3>'
		)
	);
 
	// В подвале - второй сайдбар 
	register_sidebar(
		array(
			'id' => 'true_foot',
			'name' => 'Футер',
			'description' => 'Перетащите сюда виджеты, чтобы добавить их в футер.',
			'before_widget' => '<div id="%1$s" class="foot widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>'
		)
	);
}
add_action( 'widgets_init', 'true_register_wp_sidebars' );
*/

remove_filter( 'the_content', 'wpautop' );

// Функции и классы для отображения главной страницы.

class Comment{
    var $title;
    var $name;
    var $content;
    var $imgLink;
    function Comment($name1, $title1, $content1, $imgLink1){
        $this->name = $name1;
        $this->title = $title1;
        $this->content = $content1;
        $this->imgLink = $imgLink1;
    }
}

function get_comments_custom() {
    $result = array();
    
    $args = 'tag=Отзыв&order=ASC';
    $wp_query = new WP_Query( $args );
    while ( $wp_query->have_posts() ) { 
        $wp_query->the_post();
    
        $str= get_the_content();
        preg_match_all('/src="([^"]+)"/i', $str, $matches);
        $imgLink = $matches[0][0];

        array_push($result, new Comment(get_the_title(), get_the_excerpt(), strip_tags(get_the_content()), $imgLink ));
    }
    return $result;
}
    
function first_image_from_post($content){
    $imgLink = preg_match_all('/src="([^"]+)"/i', $content, $matches);
    $imgLink = $matches[0];
    echo $imgLink[0];
}

function first_image_URL_from_post($content){
    $imgLink = preg_match_all('/src="([^"]+)"/i', $content, $matches);
    $imgLink = $matches[0];
    $imgLink = str_replace('"', '', $imgLink);
    $imgLink = str_replace('src=', '', $imgLink);
    echo $imgLink[0];
}

function get_first_image_URL_from_post($content){
    $imgLink = preg_match_all('/src="([^"]+)"/i', $content, $matches);
    $imgLink = $matches[0];
    $imgLink = str_replace('"', '', $imgLink);
    $imgLink = str_replace('src=', '', $imgLink);
    return $imgLink[0];
}

function get_all_image_URLs_from_content($content){
    $imgLink = preg_match_all('/src="([^"]+)"/i', $content, $matches);
    $imgLink = $matches[0];
    $imgLink = str_replace('"', '', $imgLink);
    $imgLink = str_replace('src=', '', $imgLink);
    return $imgLink;
}

// Картинки для "Особенности"
function get_images_to_index()
{
    $content = '';
    $args = 'tag=Изображения';
    $wp_query = new WP_Query( $args );
    while ( $wp_query->have_posts() ) { 
        $wp_query->the_post();
        $content = get_the_content();
    }
    if (preg_match_all('/<img(?:\\s[^<>]*?)?\\bsrc\\s*=\\s*(?|"([^"]*)"|\'([^\']*)\'|([^<>\'"\\s]*))[^<>]*>/i', $content, $result))
    return $result[1];
}


function get_image_to_post($post_id, $image_size = 'thumbnail') {
    $post = get_post($post_id);
    
    $post_thumbnail = '';
    $post_thumbnail = get_image_with_size(get_the_content(), $image_size);
    
    if($post_thumbnail == ''){
        $video = get_post_meta($post->ID, 'video', true);
        if($image_size == 'thumbnail'){
            $post_thumbnail = 'http://img.youtube.com/vi/'.$video.'/mqdefault.jpg';
        }
        elseif($image_size == 'medium'){
            $post_thumbnail = 'http://img.youtube.com/vi/'.$video.'/hqdefault.jpg';
        }
        else{
            $post_thumbnail = 'http://img.youtube.com/vi/'.$video.'/sddefault.jpg';
        }
    }
    return $post_thumbnail;
}

// private
function get_image_with_size($content, $image_size) {
    $image_url = get_first_image_URL_from_post($content);    
    $src = get_image_by_url($image_url, $image_size);
    return $src;
}

// private
function get_image_by_url($image_url, $size) {
    $image_id = get_image_id($image_url);
    $src = wp_get_attachment_image_src($image_id, $size );
    return $src[0];
}

// private
function get_image_id($image_url) {
    global $wpdb;
    //return $wpdb->posts;
    $attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url )); 
    return $attachment[0]; 
}

// Функции пагинации
/*
function wp_corenavi() {
global $wp_query, $wp_rewrite;
$pages = '';
$max = $wp_query->max_num_pages;
if (!$current = get_query_var('paged')) $current = 1;
$a['base'] = str_replace(999999999, '%#%', get_pagenum_link(999999999));
$a['total'] = $max;
$a['current'] = $current;
//$a['type'] = 'list';
 
$total = 0; //1 - выводить текст "Страница N из N", 0 - не выводить
$a['mid_size'] = 1; //сколько ссылок показывать слева и справа от текущей
$a['end_size'] = 1; //сколько ссылок показывать в начале и в конце
$a['prev_text'] = ''; //текст ссылки "Предыдущая страница"
$a['next_text'] = ''; //текст ссылки "Следующая страница"
 
if ($max > 1) echo '<nav class="navigation">';
if ($total == 1 && $max > 1) $pages = '<span class="pages">Страница ' . $current . ' из ' . $max . '</span>'."\r\n";
echo $pages . paginate_links($a);
if ($max > 1) echo '</nav>';

//echo '<br><br>';
//echo '---'.get_pagenum_link(2).'---';
//echo '<br><br>';
}
*/

// Функция пагинации
function wp_ildar_pagination() {
    
global $wp_query, $wp_rewrite;
if (!$current = get_query_var('paged')) $current = 1;
$max = $wp_query->max_num_pages;
if($max <= 1 ) { return; }

echo '<div class="pagination"> <ul class="pagination__list clearfix">';
$page = 1;
while ($page <= $max){
    if ($page != $current){
        echo '<li><a href="'.get_pagenum_link($page).'">'.$page.'</a></li>'; 
    }
    $page++;
} 
echo '</ul> </div>';
}
?>