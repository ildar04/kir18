<?php 
get_header();
?>   

<?php
    // Start the loop.
    while ( have_posts() ) : the_post();
    //echo "ПОСТ!!!";
    // Include the page content template.
    //get_template_part('content.php'); // вызов content.php
?>
                    
<section class="section-block"> 
    <div class="section-title"> 
        <h2> 
            <?php the_title(); ?>
        </h2> 
    </div>
    <?php
        the_content();
    ?>
    
    <?php
// Вызывает кнопку "Изменить страницу" если возможно
		edit_post_link(
			sprintf(
				/* translators: %s: Name of current post */
				__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
				get_the_title()
			),
			'<footer class="entry-footer"><span class="edit-link">',
			'</span></footer><!-- .entry-footer -->'
		);
	?>
    
</section>  

<?php
    endwhile;
?>

<?php
get_footer(); 
?>